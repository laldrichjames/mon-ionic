import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClassSchedPage } from './class.sched.page';

const routes: Routes = [
  {
    path: '',
    component: ClassSchedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClassSchedPageRoutingModule {}
