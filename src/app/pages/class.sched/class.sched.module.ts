import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ClassSchedPageRoutingModule } from './class.sched-routing.module';

import { ClassSchedPage } from './class.sched.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ClassSchedPageRoutingModule
  ],
  declarations: [ClassSchedPage]
})
export class ClassSchedPageModule {}
