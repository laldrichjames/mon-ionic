import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudBalPage } from './stud.bal.page';

const routes: Routes = [
  {
    path: '',
    component: StudBalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StudBalPageRoutingModule {}
