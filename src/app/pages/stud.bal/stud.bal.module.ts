import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StudBalPageRoutingModule } from './stud.bal-routing.module';

import { StudBalPage } from './stud.bal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StudBalPageRoutingModule
  ],
  declarations: [StudBalPage]
})
export class StudBalPageModule {}
